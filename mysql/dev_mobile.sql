-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2022 at 09:47 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_mobile`
--

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `order_date` datetime DEFAULT NULL,
  `customerUsername` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `order_date`, `customerUsername`) VALUES
(1, '2022-04-24 00:00:00', 'nguoidung01'),
(2, '2022-04-21 00:00:00', 'nguoidung02'),
(3, '2022-04-22 00:00:00', 'nguoidung03'),
(4, '2022-04-21 00:00:00', 'nguoidung04'),
(5, '2022-04-28 00:00:00', 'nguoidung05'),
(6, '2022-04-25 00:00:00', 'nguoidung01'),
(7, '2022-04-22 00:00:00', 'nguoidung02'),
(8, '2022-04-29 00:00:00', 'nguoidung03'),
(9, '2022-04-29 00:00:00', 'nguoidung04'),
(10, '2022-04-24 00:00:00', 'nguoidung05');

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `quantityOrdered` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `productId` int(11) DEFAULT NULL,
  `orderId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id`, `quantityOrdered`, `productId`, `orderId`) VALUES
(1, 3, 22, 1),
(2, 2, 11, 2),
(3, 3, 22, 2),
(4, 1, 23, 2),
(5, 3, 10, 3),
(6, 3, 22, 3),
(7, 2, 10, 4),
(8, 2, 17, 4),
(9, 2, 25, 5),
(10, 3, 5, 5),
(11, 2, 1, 6),
(12, 3, 14, 6),
(13, 1, 5, 7),
(14, 3, 13, 7),
(15, 3, 13, 8),
(16, 3, 6, 8),
(17, 1, 23, 9),
(18, 3, 1, 9),
(19, 3, 7, 10),
(20, 2, 23, 10);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `name` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `price` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `quantityInStock` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `productLineId` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `active`, `name`, `description`, `price`, `quantityInStock`, `productLineId`) VALUES
(1, 1, 'Quần Shorts Nam I7SHP513I', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 350000, 225, 4),
(2, 1, 'Áo Polo I7POL004K', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 299000, 350, 4),
(3, 1, 'Áo Polo Nam I7POL003K', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 450000, 275, 4),
(4, 1, 'Áo Polo Nam I7POL001K', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 299000, 300, 4),
(5, 1, 'Áo Polo GOLD Cổ Phối Sọc 2 Màu I7POL807K', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 450000, 300, 4),
(6, 1, 'Áo Polo GOLD+ Cổ Kẻ Chéo I7POL806K', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 299000, 200, 4),
(7, 1, 'Áo Polo Nam I7POL805K', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 390000, 275, 4),
(8, 1, 'Áo Polo Nam Dáng Ôm I7POL801I', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 299000, 325, 4),
(9, 1, 'Áo T-Shirt Nam E7TSH017G', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 450000, 325, 4),
(10, 1, 'Áo Polo Nam I7POL002K', 'SUNSTOP UV MASTER vải mát lạnh nhờ sử dụng sợi Rayon làm mát cơ thể giúp người mặc cảm thấy thoải mái ngay cả khi đi giữa trưa hè.\nCông nghệ chống nắng Hightech (công nghệ cao): chỉ số chống nắng UPF 50+ chặn đứng tác nhân gây đen sạm, lão hóa, ung thư da, duy trì khả năng chống UV sau nhiều lần giặt trong suốt quá trình sử dụng.\nBảo vệ tốt nhất - ngăn tới 98% tia UV được Viện Dệt May Việt Nam kiểm nghiệm và xác nhận.\nThiết kế thời trang, gọn gàng, an toàn khi di chuyển.\nBảo vệ toàn diện với Áo khoác, Chân váy, Găng tay, khẩu trang.\nĐầy đủ cho cả gia đình (Nam, nữ, trẻ em).\nCam kết 1 đổi 1 trong 14 ngày nếu Quý Khách không hài lòng.', 299000, 350, 4),
(11, 1, 'Giày thể thao kháng khuẩn êm chân M9SHO225J', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 450000, 350, 5),
(12, 1, 'Áo sơ mi lanh cổ Đức I9SHI519K', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 299000, 250, 5),
(13, 1, 'Áo sơ mi lanh cổ Đức I9SHI518K', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 450000, 125, 5),
(14, 1, 'Đầm Nữ sát nách gấu bèo I9DRE532I', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 299000, 300, 5),
(15, 1, 'Đầm Nữ cổ tim dáng A I9DRE538I', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 390000, 125, 5),
(16, 1, 'Đầm Nữ cổ Đức dáng sơ mi I9DRE534I', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 245000, 125, 5),
(17, 1, 'Đầm Nữ cổ đức dáng sơ mi I9DRE520I', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 350000, 150, 5),
(18, 1, 'Quần dài Nữ I9PAN556K', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 299000, 275, 5),
(19, 1, 'Đầm Nữ cổ tròn dáng suông I9DRE525I', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 450000, 250, 5),
(20, 1, 'Áo T-Shirt Nữ cổ tròn I9TSH800I', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 299000, 325, 5),
(21, 1, 'Quần Shorts Nữ I9SHP514K', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 450000, 275, 5),
(22, 1, 'Áo T-Shirt Nữ cổ tròn I9TSH612K', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 299000, 125, 5),
(23, 1, 'Áo T-Shirt Nữ cổ tròn I9TSH558K', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 450000, 125, 3),
(24, 1, 'Quần Shorts Jean Nữ I9SHP002I', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 299000, 325, 2),
(25, 1, 'Áo T-Shirt Nữ cổ tròn I9TSH557K', 'Áo sơ mi là món đồ thiết yếu của mọi chị em phụ nữ, được ưa chuộng quanh năm nhất là vào mùa xuân - hè.\nÁo sơ mi tại TokyoLife được thiết kế kiểu dáng đơn giản với nhiều chi tiết cách điệu, nhấn nhá tạo nên sự mềm mại nhưng vẫn không kém phần năng động và hiện đại.\nSản phẩm có màu sắc trung tính, nhẹ nhàng cho vẻ ngoài thanh lịch, duyên dáng.\nÁo sơ mi có thể phối với các chất liệu khác như denin,, kaki, quần âu... để đễ dàng làm mới phong cách mỗi ngày.', 450000, 150, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_line`
--

CREATE TABLE `product_line` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_line`
--

INSERT INTO `product_line` (`id`, `name`) VALUES
(1, 'Thời trang trẻ em'),
(2, 'Sức khỏe & Làm đẹp'),
(3, 'Thời trang thông minh'),
(4, 'Thời trang nam'),
(5, 'Thời trang nữ');

-- --------------------------------------------------------

--
-- Table structure for table `product_photo`
--

CREATE TABLE `product_photo` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `productId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_photo`
--

INSERT INTO `product_photo` (`id`, `url`, `productId`) VALUES
(1, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/i/7/i733-060e_1.jpg', 1),
(2, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_polo_nam_c_c_i7pol505i_ghi_m_290000_5_5.jpg', 1),
(3, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/1/2/12_11_.jpg', 1),
(4, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_polo_nam_c_c_c_ch_n_d_ng_m_i7pol501g_en_290_000.jpg_2__1_1.jpg', 1),
(5, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/n/7/n7pol003i--17.jpg', 2),
(6, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/i/7/i7pol803i_1.jpg', 2),
(7, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/n/7/n7pol007i_11_1_1.jpg', 2),
(8, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/d/s/dsc05048_5_1.jpg', 2),
(9, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_polo_nam_c_c_c_ch_n_d_ng_m_i7pol501g_en_290_000.jpg_2__1_1.jpg', 3),
(10, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/i/7/i733-061e.jpg', 4),
(11, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_polo_nam_e7pol007g_en_390k_5.jpg', 5),
(12, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_polo_nam_e7pol002g_en_350k_3_1.jpg', 6),
(13, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_polo_nam_i7pol502g_390k_qu_n_short_nam_i7shp003g_350k_3__5_1.jpg', 7),
(14, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t-shirt_nam_c_tr_n_ng_n_tay_i7tsh510i_ghi_190000.jpg', 8),
(15, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t-shirt_nam_c_tr_n_ng_n_tay_i7tsh520it_m_than_en190000_qu_n_jeans_nam_ng_ng_i7jea004ixanh-03490000.jpg.jpg', 9),
(16, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t-shirt_nam_c_tr_n_ng_n_tay_i7tsh519ighi_m190000_qu_n_shorts_khaki_nam_i7kha006gbe-01290000.jpg', 10),
(17, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/g/i/gi_y_th_thao_n_si_u_nh_m_ch_n_n9sho202i-001-_-590.000_2_.jpg', 11),
(18, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/h/n/hnet.com-image_-_2021-12-15t163113.729_1.jpg', 12),
(19, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/n/9/n9pol003i-_2_.jpg', 13),
(20, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_kho_c_ch_ng_n_ng_uv_air_n_d_ng_d_i_f9uvc016i-002_-ok.jpg', 14),
(21, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t-shirt_n_e9tsh023g_tr_ng_02_350_000.jpg', 15),
(22, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/q/u/qu_n_joggers_n_e9pan601g_tr_ng-00_590_000_o_t-shirt_n_e9tsh032g_xanh-00_390_000_2_.jpg', 16),
(23, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t_shirt_n_e9tsh048g_tr_ng_190k_qu_n_shorts_n_e9shp403g_xanh_denim_590k_3_.jpg', 17),
(24, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t_shirt_n_e9tsh021g_cam.jpg', 18),
(25, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t_shirt_n_e9tsh015g_290k_qu_n_shorts_n_jean_e9shp401g_xanh_denim_450k_3_.jpg', 19),
(26, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t-shirt_n_e9tsh034g_tr_ng-01_250_000_ch_n_v_y_jean_n_e9skr007h_xanh-01_390_000_.jpg.jpg_2__4.jpg', 20),
(27, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/m/_m_n_s_t_n_ch_d_ng_m_i9dre543i_en190000.jpg.jpg', 21),
(28, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/t/e/test.jpg', 22),
(29, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t-shirt_n_c_tr_n_tay_xo_n_i9tsh545i_k_cam_190000.jpg', 23),
(30, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t-shirt_n_c_tim_i9tsh539i_190000_qu_n_shorts_jean_n_i9shp001i_tr_ng_29000.jpg', 24),
(31, 'https://cdn.tokyolife.com.vn/forlife/media/catalog/product/cache/61a8c7eb4804248abfa4aef0c8bbd396/_/o/_o_t_shirt_n_e9tsh014g_tr_ng_290k.jpg', 25);

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` int(11) NOT NULL,
  `size` varchar(255) NOT NULL,
  `productId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `size`, `productId`) VALUES
(1, 'S', 1),
(2, 'L', 1),
(3, 'M', 1),
(4, 'XL', 1),
(5, 'S', 2),
(6, 'M', 2),
(7, 'XL', 2),
(8, 'XXL', 2),
(9, 'M  ', 3),
(10, 'M  ', 4),
(11, 'L  ', 5),
(12, 'XL  ', 6),
(13, 'XXL  ', 7),
(14, 'XXL  ', 8),
(15, 'XL  ', 9),
(16, 'L  ', 10),
(17, 'XXL  ', 11),
(18, 'XXL  ', 12),
(19, 'XL  ', 13),
(20, 'S', 14),
(21, 'XXL', 15),
(22, 'L  ', 16),
(23, 'XXL  ', 17),
(24, 'XL  ', 18),
(25, 'XL  ', 19),
(26, 'XL  ', 20),
(27, 'XL  ', 21),
(28, 'XL  ', 22),
(29, 'M  ', 23),
(30, 'L  ', 24),
(31, 'S  ', 25);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `email` varchar(125) DEFAULT NULL,
  `googleId` text DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(125) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `email`, `googleId`, `password`, `role`) VALUES
('admin', NULL, NULL, 'admin', 'admin'),
('nguoidung01', NULL, NULL, 'nguoidung01', 'customer'),
('nguoidung02', NULL, NULL, 'nguoidung02', 'customer'),
('nguoidung03', NULL, NULL, 'nguoidung03', 'customer'),
('nguoidung04', NULL, NULL, 'nguoidung04', 'customer'),
('nguoidung05', NULL, NULL, 'nguoidung05', 'customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_c4e28f0ecdf36056a9550b2ac76` (`customerUsername`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_a3647bd11aed3cf968c9ce9b835` (`productId`),
  ADD KEY `FK_88850b85b38a8a2ded17a1f5369` (`orderId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_97d8fa57594e8632884e7e70b47` (`productLineId`);

--
-- Indexes for table `product_line`
--
ALTER TABLE `product_line`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_photo`
--
ALTER TABLE `product_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_e29118b4b3fb53584548fc80626` (`productId`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_013d7ffd083e76fcd6fe815017c` (`productId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `product_line`
--
ALTER TABLE `product_line`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_photo`
--
ALTER TABLE `product_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK_c4e28f0ecdf36056a9550b2ac76` FOREIGN KEY (`customerUsername`) REFERENCES `user` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `FK_88850b85b38a8a2ded17a1f5369` FOREIGN KEY (`orderId`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_a3647bd11aed3cf968c9ce9b835` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_97d8fa57594e8632884e7e70b47` FOREIGN KEY (`productLineId`) REFERENCES `product_line` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_photo`
--
ALTER TABLE `product_photo`
  ADD CONSTRAINT `FK_e29118b4b3fb53584548fc80626` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_size`
--
ALTER TABLE `product_size`
  ADD CONSTRAINT `FK_013d7ffd083e76fcd6fe815017c` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
